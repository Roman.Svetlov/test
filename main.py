import sys
from datetime import datetime

path = sys.argv[1]


def checkStates():
    States = {}
    with open(path) as file:
        for line in file:
            if "State" in line:
                if States.get(line) == None:
                    States[line] = 1
                else:
                    States.update({line: States[line] + 1})
    print(States)


if "-checkStates" in sys.argv:
    checkStates()


def cfPrint(*values, file):
    print(*values)
    if file != sys.stdout:
        print(*values, file=file)


recordTime = datetime.min
inputTime = datetime.min
outputTime = datetime.min
outputTimeErrorless = datetime.min
outputBites = 0
outputBitesErrorless = 0
outputBitesTotal = 0
inputBites = 0
waitingTime = datetime.min - datetime.min
waitingStart = datetime.min
firstSuccessful = []
lastSuccessful = []
successfulCycles = 0
recordCounter = 0
recordCounterLast = 0
with open(path) as file:
    for line in file:
        if line[0].isdigit() and line[2] == '.':  # Так быстрее, чем проверять каждую строку на дату
            try:
                recordTime = datetime.strptime(line[0:-1], "%d.%m.%Y %H:%M:%S.%f")
                recordCounter += 1
            except ValueError:
                print(ValueError)
        if recordCounter == recordCounterLast:  # Читаем строки, которые не время
            if "Передача" in line:
                outputTime = recordTime
                outputBites = int(line.split()[1])
                outputBitesTotal += outputBites
            if "State=ssWaitAnswer" in line:
                waitingStart = recordTime
            if "Чтение данных" in line:
                if waitingStart != datetime.min:
                    waitingTime += recordTime - waitingStart
            if "Принято" in line:
                inputBites += int(line.split()[-2])
                outputTimeErrorless = outputTime
                if inputTime == datetime.min:
                    inputTime = recordTime
                    firstSuccessful.append(outputTimeErrorless)
                    firstSuccessful.append(inputTime)
                inputTime = recordTime
                outputBitesErrorless += outputBites
                successfulCycles += 1

        # Считаем и разделяем записи
        if recordTime != datetime.min or recordCounterLast != recordCounter:
            recordCounterLast = recordCounter

lastSuccessful.append(outputTimeErrorless)
lastSuccessful.append(inputTime)

if len(sys.argv) > 2:
    outPath = sys.argv[2]
    if len(sys.argv) > 3:
        if "-a" in sys.argv:
            file = open(outPath, 'a', encoding='utf-8')
    else:
        file = open(outPath, 'w', encoding='utf-8')
else:
    file = sys.stdout
cfPrint(" * " * 3, datetime.now().isoformat(), " * " * 3, file=file)
cfPrint(" * " * 3, path, " * " * 3, file=file)
cfPrint(f'Количество успешных обменов: {successfulCycles}', file=file)
cfPrint(
    f'Передано {outputBitesErrorless} байт успешно, {outputBitesTotal} байт всего, {outputBitesTotal - outputBitesErrorless} байт отправлено без ответа',
    file=file)
cfPrint(f'Получено {inputBites} байт всего', file=file)
cfPrint(f'Интервал простоя: {waitingTime}', file=file)
cfPrint(f'Первый успешный цикл: с {firstSuccessful[0].isoformat()} по {firstSuccessful[1].isoformat()}', file=file)
cfPrint(f'Последний успешный цикл: с {lastSuccessful[0].isoformat()} по {lastSuccessful[1].isoformat()}', file=file)
cfPrint(f'Всего записей прочитано: {recordCounter}', file=file)
cfPrint(" * " * 15, file=file)
file.close()