    # Написать утилиту,
    # определяющую на основе данных
    # из файла отладки канального уровня (ServerSocket1.txt),
    # следующие величины:
    # 1) Кол-во циклов успешных обменов. ✔
    #     Примем, успешный цикл обмена: "Передача -> Принято" без "Ошибка" промеж них.
    # 2) Кол-во байт на передачу. ✔
    #     Примем: сумма всех "Передача {x}" на момент успешного приёма
    # 3) Кол-во байт на прием. ✔
    #     Примем: сумма всех "Принято в Socket->ReceiveBuf() {x}"
    # 4) Интервал простоя. (сколько времени канал провел без ответа от удаленной стороны) ✔
    #     Примем, интервал простоя это время Чтение данных - State=ssWaitAnswer
    # 5) Время первого успешного обмена. ✔
    #     Запишем диапазон от Передача до Принято.
    # 6) Время последнего успешного обмена. ✔
    #     Запишем диапазон от Передача до Принято.
    # Существующие состояния:
    # {' State=ssIdle\n': 18038, ' State=ssConnected\n': 120, ' State=ssWaitAnswer\n': 18040, ' State=ssDisconnected\n': 240}


```
 *  *  *  2024-07-04T23:37:53.583517  *  *  * 
 *  *  *  C:\!test\ServerSocket1.txt  *  *  * 
Количество успешных обменов: 17893
Передано 1414730 байт успешно, 1426254 байт всего, 11524 байт отправлено без ответа
Получено 516896 байт всего
Интервал простоя: 1:25:45.009000
Первый успешный цикл: с 2016-07-03T01:07:57.305000 по 2016-07-03T01:07:57.700000
Последний успешный цикл: с 2016-07-03T12:03:53.888000 по 2016-07-03T12:03:54.206000
Всего записей прочитано: 92942
 *  *  *  *  *  *  *  *  *  *  *  *  *  *  * 
```



