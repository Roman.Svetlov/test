﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Globalization;

namespace testtesttest
{
    internal class Program
    {
        static void checkStates(string path)
        {
            Dictionary<string, int> states = new Dictionary<string, int>();
            StreamReader sr = new StreamReader(path);
            while (!sr.EndOfStream)
            {
                string line = sr.ReadLine();
                if (line.Contains("State"))
                {
                    if (states.ContainsKey(line))
                    {
                        states[line] += 1;
                    }
                    else
                    {
                        states.Add(line, 1);
                    }
                }
            }
            sr.Close();
            states.Select(i => $"{i.Key}: {i.Value}").ToList().ForEach(Console.WriteLine);
        }
        static void cfPrint(string str, StreamWriter sw)
        {
            Console.WriteLine(str);
            sw.WriteLine(str);
        }
        static void Main(string[] args)
        {
            DateTime startTime = DateTime.Now;

            string path;
            string outPath;
            bool append = false;

            path = args[0];
            outPath = args[1];

            if (args.Contains("-a"))
                append = true;
            if (args.Contains("--checkStates"))
                checkStates(path);


            DateTime recordTime = DateTime.MinValue;
            DateTime inputTime = DateTime.MinValue;
            DateTime outputTime = DateTime.MinValue;
            DateTime outputTimeErrorless = DateTime.MinValue;
            DateTime waitingStart = DateTime.MinValue;
            TimeSpan waitingTime = TimeSpan.Zero;

            int outputBytes = 0;
            int outputBytesErrorless = 0;
            int outputBytesTotal = 0;
            int inputBytes = 0;


            List<DateTime> firstSuccessful = new List<DateTime>();
            List<DateTime> lastSuccessful = new List<DateTime>();
            int successfulCycles = 0;
            int recordCounter = 0;

            StreamReader sr = new StreamReader(path, Encoding.Default);
            while (!sr.EndOfStream)
            {
                string line = sr.ReadLine();
                if (line.Length > 1 && int.TryParse(line[0].ToString(), out _) && line[2] == '.')
                {
                    if (DateTime.TryParse(line, out recordTime))
                    {
                        recordCounter++;
                    }
                }
                else
                {
                    if (line.Contains("Передача"))
                    {
                        outputTime = recordTime;
                        outputBytes = int.Parse(line.Split()[1]);
                        outputBytesTotal += outputBytes;
                    }

                    if (line.Contains("State=ssWaitAnswer"))
                    {
                        waitingStart = recordTime;
                    }

                    if (line.Contains("Чтение данных"))
                        if (!waitingStart.Equals(DateTime.MinValue))
                            waitingTime += recordTime - waitingStart;

                    if (line.Contains("Принято"))
                    {
                        var splittedLine = line.Split();
                        inputBytes += int.Parse(splittedLine[splittedLine.Length - 2]);
                        outputTimeErrorless = outputTime;
                        if (inputTime.Equals(DateTime.MinValue))
                        {
                            inputTime = recordTime;
                            firstSuccessful.Add(outputTimeErrorless);
                            firstSuccessful.Add(inputTime);
                        }

                        inputTime = recordTime;
                        outputBytesErrorless += outputBytes;
                        successfulCycles += 1;
                    }
                }
            }
            lastSuccessful.Add(outputTimeErrorless);
            lastSuccessful.Add(inputTime);


            using (StreamWriter sw = new StreamWriter(outPath, append, Encoding.UTF8))
            {
                cfPrint($" *  *  * {DateTime.Now.ToString("o", CultureInfo.InvariantCulture),-35} *  *  * ", sw);
                cfPrint($" *  *  * {path,-35} *  *  * ", sw);
                cfPrint($"Количество успешных обменов: {successfulCycles}", sw);
                cfPrint(
                    $"Передано {outputBytesErrorless} байт успешно, {outputBytesTotal} байт всего, {outputBytesTotal - outputBytesErrorless} байт отправлено без ответа", sw);
                cfPrint($"Получено {inputBytes} байт всего", sw);
                cfPrint($"Интервал простоя: {waitingTime}", sw);
                cfPrint($"Первый успешный цикл: с {firstSuccessful[0].ToString("o", CultureInfo.InvariantCulture)} по {firstSuccessful[1].ToString("o", CultureInfo.InvariantCulture)}", sw);
                cfPrint($"Последний успешный цикл: с {lastSuccessful[0].ToString("o", CultureInfo.InvariantCulture)} по {lastSuccessful[1].ToString("o", CultureInfo.InvariantCulture)}", sw);
                cfPrint($"Всего записей прочитано: {recordCounter}", sw);
                cfPrint($" *  *  * {$"Выполнено за {(DateTime.Now - startTime).TotalSeconds} секунд",-35} *  *  * ", sw);
            }


        }
    }
}
